How to get Setup (or something close to it):
get Eclipse
add subclipse plugin (need to get from Web)
*optionally add visual editor plugin (need to add from web)
Right click in package explorer
New->Other->Checkout Projects from SVN (Next)
Create a new repository location (Next)
http://bwidawsk.dyndns.org:8008/svn/bengo (Next, sanath/sanath)
Select trunk (Next)
(Finish)


Project packages:
exceptions.* - exceptions
game.* - code implementing the game of Go
graphics - some of the main GUI (unfortunately I got lazy and GUI code exists everywhere)
graphics.twod - java 2d specific code
graphics.threed - to be implemented
input.* - various mechanisms to provide input to the game (only network and GUI have been touched recently)
input.AI - eventually AI stuff would go here
network - support a networked client/server model, big work in progress. Has some GUI stuff for now
other - Stuff that hasn't yet been sorted, most important of which is probably main()

Notes:
I haven't really thought this all through as thoroughy as I should have...
I've probably spent a total of 20 hours on this, so it's not very good