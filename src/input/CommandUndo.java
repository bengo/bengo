package input;

import game.Board;
import game.Player;
import other.Game;
import other.Move;
import other.UndoMove;

public class CommandUndo extends InputCommand {
	private Board board;
	public CommandUndo(Board b) {
		super();
		this.commandShouldReturn = true;
		this.board = b;
	}

	@Override
	public Move dispatch(Game g, Player p) {
		return new UndoMove(this.board, null, p);
	}
	
}
