package input;

import game.Player;
import other.Game;
import other.GameOptions;
import other.Move;

public class AIPlayerInput implements PlayerInput {

	int moveNo = -1;

	@Override
	public Move getMove(Game g, Player p) {
		moveNo++;
		int size = GameOptions.getInstance().getBoardSize();
		return new Move(null, g.getBoard().getPoint(moveNo % size, (moveNo / size) % size), p);
	}

	@Override
	public void handleException(Exception e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyBoardUpdate(Move m) {
		// TODO Auto-generated method stub
		
	}
}
