package input;

import game.Player;
import other.Game;
import other.Move;

public class CommandGenericNoReturn extends InputCommand {
	public CommandGenericNoReturn() {
		super();
		this.commandShouldReturn = false;
	}

	@Override
	public Move dispatch(Game g, Player p) {
		// TODO Auto-generated method stub
		return null;
	}

}
