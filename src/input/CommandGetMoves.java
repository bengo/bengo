package input;

import game.Player;
import graphics.GuiMain;
import other.Game;
import other.LocalGame;
import other.Move;

public class CommandGetMoves extends InputCommand {
	LocalGame game;
	
	@Override
	public Move dispatch(Game g, Player p) {
		//TODO make this not local specific (requires re-design)
		this.game = (LocalGame) g;
		synchronized(GuiMain.actionOccured) {
			GuiMain.actionOccured.returnVal = ((LocalGame) g).getMovesStack();
		}
		return null;
	}

	@Override
	public String toString() {
		return game.getMovesStack().toString();
	}
	
}
