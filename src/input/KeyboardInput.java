package input;

import game.Player;
import graphics.AsciiDisplay;
import other.Game;
import other.Move;

public class KeyboardInput implements PlayerInput {
	
	AsciiDisplay ad = new AsciiDisplay();
	
	public KeyboardInput() {
		super();
	}

	
	
	@Override
	public Move getMove(Game g, Player p) {
		boolean gotIt = false;
		
		while(!gotIt) {
			System.out.print(p + "> ");
			InputCommand command = ad.getCommand(g);
			
			if(command != null && command.shouldReturn()) {
				return command.dispatch(g, p);
			} else if (command != null){
				command.dispatch(g, p);
			}
		}
        
		return null;
	}

	@Override
	public void handleException(Exception e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyBoardUpdate(Move m) {
		// TODO Auto-generated method stub
		
	}

}
