package input;

import game.Player;
import other.Game;
import other.Move;

public interface PlayerInput {
	Move getMove(Game g, Player p);
	void notifyBoardUpdate(Move m);
	void handleException(Exception e);
}
