package input;

import game.Player;
import other.BoardCoordinate;
import other.Game;
import other.Move;

public class CommandMove extends InputCommand {
	private BoardCoordinate coord;
	public CommandMove(BoardCoordinate coord) {
		super();
		this.coord = coord;
		this.commandShouldReturn = true;
	}

	public BoardCoordinate getCoord() {
		return coord;
	}

	@Override
	public Move dispatch(Game g, Player p) {
		return new Move(g.getBoard(), g.getBoard().getPoint(coord.getX(), coord.getY()), p);
	}
	
}
