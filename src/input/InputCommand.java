package input;

import java.io.Serializable;

import game.Player;
import other.Game;
import other.LocalGame;
import other.Move;

public abstract class InputCommand implements Serializable {

	protected boolean commandShouldReturn = false;
	
	/**
	 * 
	 * @param g model
	 * @param p view?
	 * @param c controller
	 * @return
	 */
	public abstract Move dispatch(Game g, Player p);
	
	public boolean shouldReturn() {
		return commandShouldReturn;
	}
	
	public static InputCommand GetCommand(Game g, String cmd) {
		InputCommand retCommand = null;
		if (cmd.equals("help")) {
			System.out.println("To make a move, type the coordinates in the form \"X.Y\"");
			System.out.println("Other commands:");
			System.out.println("\tshowboard: shows the board");
			System.out.println("\tshowcons: shows the existing connections");
			retCommand = new CommandHelp();
		}
		
		if (cmd.equals("showboard")) {
			//g.board.drawAscii();
			retCommand = new CommandGenericNoReturn();
		}
		
		if (cmd.equals("showcons")) {
			g.getBoard().showConnections();
			retCommand = new CommandGenericNoReturn();
		}
		
		if (cmd.equals("showmoves")) {
			//TODO remove cast
			System.out.println(((LocalGame) g).getMovesStack());
			retCommand = new CommandGetMoves();
		}
		
		if (cmd.equals("undo")) {
			retCommand = new CommandUndo(g.getBoard());
		}

		return retCommand;
	}
	
	
}
