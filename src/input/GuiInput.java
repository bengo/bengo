package input;


import exceptions.UndoException;
import game.Player;
import graphics.GuiMain;
import other.Game;
import other.Move;

public class GuiInput implements PlayerInput {
	
	public GuiInput() {
		super();
	}

	@Override
	synchronized public Move getMove(Game g, Player p) {
		try {
			synchronized(GuiMain.actionOccured) {
				while(true) {
					GuiMain.actionOccured.wait();
					InputCommand command = GuiMain.getInstance(null).getCommand(g);
					if(command != null && command.shouldReturn()) {
						return command.dispatch(g, p);
					} else if (command != null){
						command.dispatch(g, p);
						if(GuiMain.actionOccured.expectsReturn) {
							GuiMain.actionOccured.notify();
						}
					}
				}
				//return new Move(g.board, GuiMain.lastBoardCoord, p);
			}
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
		return null;
	}

	@Override
	public void handleException(Exception e) {
		if(e instanceof UndoException) {
			return;
		}
		GuiMain.getInstance(null).popupError(e.getMessage());

	}

	@Override
	public void notifyBoardUpdate(Move m) {
		GuiMain.getInstance(null).moveUpdate(m);
	}
}
