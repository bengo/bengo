package exceptions;

public class KoException extends Exception {

	@Override
	public String getMessage() {
		return "ko - A play is illegal if it would have the effect (after all steps of the play have been completed) of creating a position that has occurred previously in the game.";
	}

}
