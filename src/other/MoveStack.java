package other;

import java.util.Stack;

public class MoveStack extends Stack<Move>{

	@Override
	public synchronized String toString() {
		String retString = new String();
		for(int i = 0; i < this.size(); i++)
			retString += this.get(i).toString();
		
		return retString;
	}

	@Override
	public synchronized Move pop() {
		return super.pop();
	}

	@Override
	public Move push(Move item) {
		return super.push(item);
	}

}
