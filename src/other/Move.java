package other;

import exceptions.AlreadyOccupiedException;
import exceptions.SuicideException;
import exceptions.UndoException;
import game.BengoPoint;
import game.Board;
import game.BoardState;
import game.Connection;
import game.Player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Stack;

public class Move implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5059508104144132209L;
	BengoPoint moveCoord;
	/* connection which has been created as a result of the move */
	private Connection newConnection;
	/* connection which have been merged */
	private ArrayList<Connection> oldConnections;
	/* connections which were captured */
	private ArrayList<Connection> capturedConnection;
	private ArrayList<Connection> oldEmptyConnections;
	int moveNo = 0;

	protected Player player;

	BoardState bs;
	protected Board board;

	public Move() {
		// TODO Auto-generated constructor stub
	}
	public Move(Board board, BengoPoint b, Player p) {
		super();
		this.moveCoord = b;
		this.player = p;
		this.board = board;
	}
	public Move(Board board, BoardCoordinate bc, Player p) {
		super();
		this.player = p;
		this.board = board;
		this.moveCoord = board.getPoint(bc.getX(), bc.getY());
	}

	public Move(int moveNo) {
		super();
		this.moveNo = moveNo;
	}
	
	public Move dispatch(MoveStack moveStack) throws AlreadyOccupiedException, UndoException, SuicideException {
		Move returnMove =  board.makeMove(this);
		for(Connection c : returnMove.getCapturedConnection())
			player.addCaptured(c.size());
		
		return returnMove;
	}

	public BoardState getBs() {
		return bs;
	}

	public ArrayList<Connection> getCapturedConnection() {
		return capturedConnection;
	}

	public BengoPoint getMoveCoord() {
		return moveCoord;
	}

	public int getMoveNo() {
		return moveNo;
	}

	public Connection getNewConnection() {
		return newConnection;
	}

	public ArrayList<Connection> getOldConnections() {
		return oldConnections;
	}

	public ArrayList<Connection> getOldEmptyConnections() {
		return oldEmptyConnections;
	}

	public Player getPlayer() {
		return player;
	}

	public void setBs(BoardState bs) {
		this.bs = bs;
	}

	public void setCapturedConnection(ArrayList<Connection> capturedConnection) {
		this.capturedConnection = capturedConnection;
	}

	public void setMoveCoord(BengoPoint moveCoord) {
		this.moveCoord = moveCoord;
	}

	public void setNewConnection(Connection newConnection) {
		this.newConnection = newConnection;
	}

	public void setOldConnections(ArrayList<Connection> oldConnections) {
		this.oldConnections = oldConnections;
	}

	public void setOldEmptyConnections(ArrayList<Connection> oldEmptyConnections) {
		this.oldEmptyConnections = oldEmptyConnections;
	}
	
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public void incMoveNo() {
		this.moveNo++;
	}

	@Override
	public String toString() {
		String retString = Integer.toString(moveNo);
		retString += ": " + player;
		retString += "\n\tNew: " + getNewConnection();
		//retString += "\n\tOld: " + getOldConnections();
		retString += "\n\tCaptured: " + getCapturedConnection();
		retString += "\n";
		return retString;
	}
	
}
