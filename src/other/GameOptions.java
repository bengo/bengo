package other;

import input.PlayerInput;
import input.GuiInput;

public class GameOptions {
	private GameOptions() {}
	
	private static class GameOptionsHolder { 
		private static final GameOptions INSTANCE = new GameOptions();
	}
	
	public static GameOptions getInstance() {
		return GameOptionsHolder.INSTANCE;
	}
	
	/* some default game options */
	private int boardSize = 4;
	private boolean koAllowed = false;
	private boolean suicideAllowed = false;

	//private PlayerInput inputWhite = new KeyboardInput();
	private PlayerInput inputBlack = new GuiInput();
	private PlayerInput inputWhite = inputBlack;
	private int blackHandicap = 0;
	
	public boolean isKoAllowed() {
		return koAllowed;
	}

	public PlayerInput getInputWhite() {
		return inputWhite;
	}

	public PlayerInput getInputBlack() {
		return inputBlack;
	}

	public int getBlackHandicap() {
		return blackHandicap;
	}

	public void setKoAllowed(boolean koAllowed) {
		this.koAllowed = koAllowed;
	}

	public void setInputWhite(PlayerInput inputWhite) {
		this.inputWhite = inputWhite;
	}

	public void setInputBlack(PlayerInput inputBlack) {
		this.inputBlack = inputBlack;
	}

	public void setBlackHandicap(int blackHandicap) {
		this.blackHandicap = blackHandicap;
	}

	public int getBoardSize() {
		return boardSize;
	}
	
	public boolean isSuicideAllowed() {
		return suicideAllowed;
	}

	public void setSuicideAllowed(boolean suicideAllowed) {
		this.suicideAllowed = suicideAllowed;
	}
}
