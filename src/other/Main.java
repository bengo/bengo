package other;

import javax.swing.JOptionPane;

import network.ClientGame;
import network.GameServer;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
			int n = 0;
			Game g = null;
			if (args.length == 0) {
				// assume it isn't command line, popup a GUI to
				// ask what kind of instance this will be.
				Object[] options = {"Local",
									"Server",
									"Client"};
				n = JOptionPane.showOptionDialog(null, "What will it be?", "Type", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
			}
			switch (n) {
			case JOptionPane.YES_OPTION:
				g = new LocalGame();
				break;
			case JOptionPane.NO_OPTION:
				g = new GameServer(GameOptions.getInstance());
				break;
			case JOptionPane.CANCEL_OPTION:
				g = new ClientGame();
				break;
				default:
					System.out.println("Unsupported game type");
			}
			g.play();
	}
}
