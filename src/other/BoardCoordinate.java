package other;

import java.io.Serializable;

public class BoardCoordinate implements Serializable {
	private int x;
	private int y;
	
	
	public BoardCoordinate() {
		super();
	}


	public BoardCoordinate(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}


	public int getX() {
		return x;
	}


	public int getY() {
		return y;
	}


	@Override
	public String toString() {
		return x + "," + y;
	}


	public void setX(int x) {
		this.x = x;
	}


	public void setY(int y) {
		this.y = y;
	}


	@Override
	public boolean equals(Object obj) {
		if(obj instanceof BoardCoordinate) {
			BoardCoordinate bc = (BoardCoordinate) obj;
			return (bc.x == this.x && bc.y == this.y);
		}
		
		return false;
	}
	
}
