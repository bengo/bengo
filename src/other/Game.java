package other;

import game.Board;

public interface Game {
	public void play();
	public Board getBoard();
}
