package other;

import java.util.Stack;

import exceptions.AlreadyOccupiedException;
import exceptions.UndoException;
import game.BengoPoint;
import game.Board;
import game.Connection;
import game.Player;

public class UndoMove extends Move {

	public UndoMove(Board board, BengoPoint b, Player p) {
		super(board, b, p);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 286859087753640296L;

	@Override
	public Move dispatch(MoveStack moveStack) throws AlreadyOccupiedException, UndoException {
		Move exMove = null;
		if(moveStack.size() > 0) {
			exMove = moveStack.pop();
			board.undoMove(exMove);
		}
			
		for(Connection c : exMove.getCapturedConnection())
			player.addCaptured(-c.size());
		
		throw new UndoException();
	}
}
