package other;

import java.util.Stack;

import javax.swing.JOptionPane;

import exceptions.AlreadyOccupiedException;
import exceptions.KoException;
import exceptions.SuicideException;
import exceptions.UndoException;

import game.AreaScore;
import game.Board;
import game.BoardState;
import game.Connection;
import game.Player;
import game.TerritoryScore;
import graphics.GuiMain;

public class LocalGame implements Game {

	Player whitePlayer;
	Player blackPlayer;
	public Board board;
	static GameOptions options;
	boolean done;
	
	public LocalGame() {
		options = GameOptions.getInstance();
		board = new Board(options.getBoardSize());
		whitePlayer = Player.White;
		blackPlayer = Player.Black;
		whitePlayer.setOpponent(blackPlayer);
		whitePlayer.setInput(options.getInputWhite());
		blackPlayer.setOpponent(whitePlayer);
		blackPlayer.setInput(options.getInputBlack());
		startGui();
	}
	
	public boolean isDone() {
		return done;
	}
	
	public void setDone() {
		done = true;
	}
	
	private MoveStack movesStack = new MoveStack();
	/**
	 * @param args
	 */
	
	public void play() {
		Player player = blackPlayer;
		int i = 0;
		int handicapTurns = options.getBlackHandicap();
		while(!isDone()) {
			Exception e = null;
			boolean moveOn = false;
			Move m = player.getInput().getMove(this, player);
			m.moveNo = ++i;
			
			try {
				Move executedMove = m.dispatch(movesStack);
				player.getInput().notifyBoardUpdate(executedMove);
				player.getOpponent().getInput().notifyBoardUpdate(executedMove);
				if(executedMove == null) {
					// some kind of fail or undo go back
					if(m.moveNo == 1) {
						// special case for first move
						i--;
						continue;
					}
					throw new Exception();
				} else {
					movesStack.push(executedMove);
				}
			} catch (UndoException ue) {
				e = ue;
				player = player.getOpponent();
				i--;
			} catch (SuicideException sex) {
				e = sex;
			} catch (Exception ex) {
				e = ex;
				//aoe.printStackTrace();
				e.printStackTrace();
			} finally {
				if(e != null) {
					player.getInput().handleException(e);
					i--;
					moveOn = true;
				}
			}
			if(moveOn) {
				continue;
			}
			
			if(!options.isKoAllowed()) {
				if(movesStack.size() > 2) {
					BoardState bs1 = movesStack.get(movesStack.size() - 1).bs;
					BoardState bs2 = movesStack.get(movesStack.size() - 3).bs;
					if (bs1.equals(bs2)) {
						board.undoMove(movesStack.pop());
						i--;
						player.getInput().handleException(new KoException());
						continue;
					}
				}	
			}

			if(handicapTurns != 0) {
				handicapTurns--;
			} else {
				player = player.getOpponent();
			}
		}
	}
	
	private void startGui() {
		GuiMain gm = GuiMain.getInstance(this.board);
		gm.externalMain();
	}

	public Stack<Move> getMovesStack() {
		return movesStack;
	}
	
	public TerritoryScore getTerritoryScore() {
		return new TerritoryScore(Player.White, Player.Black, this.board);
	}
	
	public AreaScore getAreaScore() {
		return new AreaScore(Player.White, Player.Black, this.board);
	}

	@Override
	public Board getBoard() {
		return board;
	}
}

