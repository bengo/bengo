package graphics;

import java.awt.Color;

public interface Stone {
	public Color getColor();
}
