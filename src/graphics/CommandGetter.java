package graphics;

import input.InputCommand;
import other.Game;
import other.LocalGame;

public interface CommandGetter {
	public InputCommand getCommand(Game g);
}
