package graphics;

import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import other.MoveStack;

import java.awt.GridBagConstraints;
import java.awt.Color;

public class MovePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private JTextArea moveTextArea = null;

	/**
	 * This is the default constructor
	 */
	public MovePanel(MoveStack moves) {
		super();
		initialize();
		moveTextArea.setText(moves.toString());
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.gridx = 0;
		this.setSize(300, 200);
		this.setLayout(new GridBagLayout());
		this.add(getMoveTextArea(), gridBagConstraints);
	}

	/**
	 * This method initializes moveTextArea	
	 * 	
	 * @return javax.swing.JTextArea	
	 */
	private JTextArea getMoveTextArea() {
		if (moveTextArea == null) {
			moveTextArea = new JTextArea();
			moveTextArea.setEditable(false);
			moveTextArea.setWrapStyleWord(true);
			moveTextArea.setBackground(Color.lightGray);
			moveTextArea.setEnabled(true);
		}
		return moveTextArea;
	}

}
