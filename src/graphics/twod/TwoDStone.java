package graphics.twod;

import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import graphics.Stone;

public class TwoDStone extends Ellipse2D implements Stone {

	double height;
	double width;
	double x;
	double y;
	Color color = null;
	
	/**
	 * 
	 * @param color
	 * @param height
	 * @param width
	 * @param x point on jpanel
	 * @param y point on jpanel
	 */
	public TwoDStone(String color, double height, double width, double x, double y) {
		super();
		this.height = height;
		this.width = width;
		this.x = x - (width / 2);
		this.y = y - (height / 2);
		if( ("WHITE".compareToIgnoreCase(color)) == 0) {
			this.color = Color.white;
		} else if ("BLACK".compareToIgnoreCase(color) == 0) {
			this.color = Color.black;
		} else if ("NEUTRAL".compareToIgnoreCase(color) == 0) {
			this.color = Color.yellow;
		}
	}

	@Override
	public double getHeight() {
		return height;
	}

	@Override
	public double getWidth() {
		return width;
	}

	@Override
	public double getX() {
		return x;
	}

	@Override
	public double getY() {
		return y;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public void setFrame(double x, double y, double w, double h) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Color getColor() {
		// TODO Auto-generated method stub
		return color;
	}

	@Override
	public Rectangle2D getBounds2D() {
		// TODO Auto-generated method stub
		return null;
	}

}
