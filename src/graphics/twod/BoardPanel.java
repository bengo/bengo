package graphics.twod;

import game.Board;
import graphics.GuiMain;

import input.CommandMove;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Line2D;
import javax.swing.JPanel;

import other.BoardCoordinate;

public class BoardPanel extends JPanel implements MouseListener {
	private int lines;
	private Point[][] points;
	private final int minDistance = 20;
	private final int offsetX = 80;
	private final int offsetY = 80;
	private int boardWidth;
	private int boardHeight;
	private int wdist;
	private int hdist;
	Board board;
	
	// vertical lines
	public BoardPanel(Board b) {
		super();
		this.setBackground(new Color(0xa6, 0x80, 0x64));
		board = b;
		this.lines = b.getSize();
		points = new Point[lines][lines];
		
		// get references to all of the game points
		for(int i = 0; i < this.lines; i++) {
			for(int j = 0; j < this.lines; j++) {
				points[i][j] = new Point();
			}
		}
		
		this.addMouseListener(this);
	}

	private void generatePoints(Graphics2D g2, int offsetX, int offsetY, int wdist, int hdist) {
		for(int i = 0; i < this.lines; i++) {
			for(int j = 0; j < this.lines; j++) {
				points[i][j].setLocation(new Point(offsetX + (wdist * (i)), offsetY +  (hdist * (j))));
				if(board.getPoint(i, j).isOccupied()) {
					int elipseSize = Math.min(hdist, wdist);
					//elipseSize /= 2;
					TwoDStone ts = new TwoDStone(board.getPoint(i, j).getOwner().toString(), elipseSize, elipseSize, points[i][j].getX(), points[i][j].getY());
					g2.setColor(ts.getColor());
					g2.fill(ts);
				}
			}
		}
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		Graphics2D g2 = (Graphics2D)g;
	    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
	    g2.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
	    g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, (RenderingHints.VALUE_TEXT_ANTIALIAS_GASP));

	    Dimension d = this.getSize();
	    /* BAW these were doubles and the division was WAY off... wtf */

		boardWidth = (int)d.getWidth() - (offsetX * 2); // padding on each side
		boardHeight = (int)d.getHeight() - (offsetY * 2); // padding on each side

		// fix up the division fudge
		boardWidth -= boardWidth % (this.lines - 1);
		boardHeight -= boardHeight % (this.lines - 1);
		
		wdist = (int)(boardWidth)/ (this.lines - 1);
		hdist = (int)(boardHeight)/ (this.lines - 1);

		Line2D.Double[] vertLines = new Line2D.Double[this.lines];
		Line2D.Double[] horizLines = new Line2D.Double[this.lines];
		for(int i = 0; i < this.lines; i++) {
			vertLines[i] = new Line2D.Double(
					offsetX + (wdist * (i)), 
					offsetY, 
					offsetX +  (wdist * (i)), 
					offsetY + boardHeight);
			horizLines[i] = new Line2D.Double(
					offsetX, 
					offsetY+ (hdist * (i)), 
					offsetX +  boardWidth, 
					offsetY + (hdist * (i)));
		}
		// Draw the numbers
		//drawNumbers(g, g2);

		for(Line2D.Double l: vertLines) {
			g2.draw(l);
		}
		for(Line2D.Double l: horizLines) {
			g2.draw(l);
		}
		generatePoints(g2, offsetX, offsetY, wdist, hdist);
	 }

	private void drawNumbers(Graphics g, Graphics2D g2) {
		for(int i = 0; i < this.lines; i++) {
			
			int fontSize = this.boardWidth / 20;
			fontSize = Math.min(offsetY, fontSize);
			fontSize = Math.min(offsetX, fontSize);
			Font font = new Font("Dialog", Font.PLAIN, fontSize);
		    // get metrics from the graphics
		    FontMetrics metrics = g.getFontMetrics(font);
		    // get the height of a line of text in this font and render context
		    int hgt = metrics.getHeight();
		    // get the advance of my text in this font and render context
		    int adv = metrics.stringWidth(Integer.toString(i));
		    // calculate the size of a box to hold the text with some padding.
		    Dimension size = new Dimension(adv+2, hgt+2);

		    g2.setFont(font);
		    if(i == 0) {
		    	// just draw the 0
		    	g2.drawString(Integer.toString(i), offsetX - adv - 5 + (wdist * (i)), offsetY - 5);
		    } else {
		    	// the fonts seem to be centered kind of funny in the first case
		    	// for horizontal lines
		    	g2.drawString(Integer.toString(i), offsetX - adv - 10, offsetY + (hgt / 2) + (hdist * (i) - 5));
		    	// for vertical lines
		    	g2.drawString(Integer.toString(i), (offsetX - adv / 2) + (wdist * (i)), offsetY - 10);		    	
		    }
		    
		}
	}

	@Override
	public Dimension getMinimumSize() {
		/* space from box to panel + space from box to lines + lines min */
		int foo = (offsetX * 2) + lines * minDistance;
		return new Dimension(foo, foo);
	}
	
	private BoardCoordinate getBoardPoint(Point p, int fudge) {
		int mDist = hdist < wdist ? hdist : wdist; 
		if(fudge <= 0 || fudge > mDist - 1) {
			fudge = mDist / 4;
		}
		int i = 0;
		boolean found = false;
		for(i = 0; i < this.lines; i++) {
			if(p.getX() == points[i][0].getX()) {
				found = true;
				break;
			}

			if(p.getX() > points[i][0].getX() && points[i][0].getX() + fudge >=  p.getX()) {
				found = true;
				break;
			}
			
			if(p.getX() < points[i][0].getX() && points[i][0].getX() - fudge <=  p.getX()) {
				found = true;
				break;
			}
		}
		if(!found) {
			return null;
		}
		int j;
		found = false;
		for(j = 0; j < this.lines; j++) {
			if(p.getY() == points[i][j].getY()) {
				found = true;
				break;
			}
			if(p.getY() > points[i][j].getY() && points[i][j].getY() + fudge >=  p.getY()) {
				found = true;
				break;
			}
			if(p.getY() < points[i][j].getY() && points[i][j].getY() - fudge <=  p.getY()) {
				found = true;
				break;
			}
		}
		if(!found) {
			return null;
		}
		
		return new BoardCoordinate(i, j);
	}
	
	@Override
	synchronized public void mouseClicked(java.awt.event.MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON3) {
			BoardCoordinate bc = getBoardPoint(e.getPoint(), 0);
			//Connection.buildConnectionFromPoint(, c)
		}
		if(e.getClickCount() > 1) {
//			System.out.println(e.getPoint());
//			System.out.println(getBoardPoint(e.getPoint(), 0));
			BoardCoordinate bc = getBoardPoint(e.getPoint(), 0);
			if (bc != null) {
				synchronized(GuiMain.actionOccured) {
					GuiMain.actionOccured.x = bc.getX();
					GuiMain.actionOccured.y = bc.getY();
					GuiMain.actionOccured.cmd = new CommandMove(bc);
					GuiMain.actionOccured.expectsReturn = false;
					GuiMain.actionOccured.notify();
					//GuiMain.lastBoardCoord.setX(bc.getX());
					//GuiMain.lastBoardCoord.setY(bc.getY());
					//GuiMain.lastBoardCoord.notify();
				}
			}
		} 
	}

	@Override
	public void mouseEntered(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}

