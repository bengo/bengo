package graphics;

import input.CommandMove;
import input.InputCommand;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import other.BoardCoordinate;
import other.Game;
import other.LocalGame;

public class AsciiDisplay implements CommandGetter {
	private BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));;
	
	public InputCommand getCommand(Game g) {
		try {
			String input = br.readLine();
			// First try to handle what we know about specific to our input
			// if that fails defer to the generic string handler
			try {
				String[] coords = input.split("\\.");
				
				if(coords.length != 2)
					return InputCommand.GetCommand(g, input);
				
				int x = Integer.parseInt(coords[0]);
				int y = Integer.parseInt(coords[1]);
				return new CommandMove(new BoardCoordinate(x, y));

			} catch (NumberFormatException nfe) {
				return InputCommand.GetCommand(g, input);
			}
		} catch(IOException ioe) {
				ioe.printStackTrace();
		}
		return null;
	}
}
