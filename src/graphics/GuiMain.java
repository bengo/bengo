package graphics;

import game.AreaScore;
import game.Board;
import game.Score;
import graphics.twod.BoardPanel;

import input.CommandGetMoves;
import input.CommandGetScore;
import input.CommandUndo;
import input.InputCommand;

import java.awt.Dialog.ModalityType;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.KeyStroke;
import java.awt.Point;
import java.util.Stack;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JFrame;
import javax.swing.JDialog;
import javax.swing.PopupFactory;

import other.Game;
import other.LocalGame;
import other.Move;
import other.MoveStack;

public class GuiMain implements CommandGetter {

	private JFrame jFrame = null;
	private JPanel boardPanel = null;
	private JMenuBar jJMenuBar = null;
	private JMenu fileMenu = null;
	private JMenu viewMenu = null;
	private JMenu cmdMenu = null;
	private JMenu helpMenu = null;
	private JMenuItem exitMenuItem = null;
	private JMenuItem aboutMenuItem = null;
	private JMenuItem viewMovesMenuItem = null;
	private JMenuItem undoMenuItem = null;
	private JMenuItem saveMenuItem = null;
	private JDialog aboutDialog = null;
	private JPanel aboutContentPane = null;
	private JLabel aboutVersionLabel = null;
	int boardSize;
	private Board board;
	
	private GuiMain() {}
	
	private static class GuiMainHolder { 
		private static final GuiMain INSTANCE = new GuiMain();
	}
	
	public static GuiMain getInstance(Board b) {
		if (b != null) {
			GuiMainHolder.INSTANCE.board = b;
		}
		return GuiMainHolder.INSTANCE;
	}
	/**
	 * This method initializes jFrame
	 * 
	 * @return javax.swing.JFrame
	 */
	private JFrame getJFrame() {
		if (jFrame == null) {
			jFrame = new JFrame();
			jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			jFrame.setJMenuBar(getJJMenuBar());
			// get the size of menus and crap
			Dimension d = jFrame.getPreferredSize();
			jFrame.setContentPane(getTwoDPanel());
			jFrame.setTitle("bengo");
			jFrame.setMinimumSize(new Dimension(
					(int)(getJFrame().getMinimumSize().getWidth() + d.getWidth()), 
					(int)(getJFrame().getMinimumSize().getHeight() + d.getHeight())));
			jFrame.setSize(jFrame.getMinimumSize());
			// kind of brown...
			jFrame.setBackground(new Color(0xa6, 0x80, 0x64));
		}
		return jFrame;
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getTwoDPanel() {
		if (boardPanel == null) {
			boardPanel = new BoardPanel(board);
			boardPanel.setLayout(new BorderLayout());
			boardPanel.setSize(boardPanel.getMinimumSize());
			//boardPanel.setBackground(new Color(0xa6, 0x80, 0x64));
			
		}
		return boardPanel;
	}

	/**
	 * This method initializes jJMenuBar	
	 * 	
	 * @return javax.swing.JMenuBar	
	 */
	private JMenuBar getJJMenuBar() {
		if (jJMenuBar == null) {
			jJMenuBar = new JMenuBar();
			jJMenuBar.add(getFileMenu());
			jJMenuBar.add(getViewMenu());
			jJMenuBar.add(getCmdMenu());
			jJMenuBar.add(getHelpMenu());
		}
		return jJMenuBar;
	}

	/**
	 * This method initializes jMenu	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getFileMenu() {
		if (fileMenu == null) {
			fileMenu = new JMenu();
			fileMenu.setText("File");
			fileMenu.add(getSaveMenuItem());
			fileMenu.add(getExitMenuItem());
		}
		return fileMenu;
	}

	/**
	 * This method initializes jMenu	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getViewMenu() {
		if (viewMenu == null) {
			viewMenu = new JMenu();
			viewMenu.setText("View");
			viewMenu.add(getViewMovesMenuItem());
		}
		return viewMenu;
	}
	
	/**
	 * This method initializes jMenu	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getCmdMenu() {
		if (cmdMenu == null) {
			cmdMenu = new JMenu();
			cmdMenu.setText("Cmd");
			cmdMenu.add(getUndoMenuItem());
			cmdMenu.add(getScoreMenuItem());
		}
		return cmdMenu;
	}

	/**
	 * This method initializes jMenu	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getHelpMenu() {
		if (helpMenu == null) {
			helpMenu = new JMenu();
			helpMenu.setText("Help");
			helpMenu.add(getAboutMenuItem());
		}
		return helpMenu;
	}

	/**
	 * This method initializes jMenuItem	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getExitMenuItem() {
		if (exitMenuItem == null) {
			exitMenuItem = new JMenuItem();
			exitMenuItem.setText("Exit");
			exitMenuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			});
		}
		return exitMenuItem;
	}

	/**
	 * This method initializes jMenuItem	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getAboutMenuItem() {
		if (aboutMenuItem == null) {
			aboutMenuItem = new JMenuItem();
			aboutMenuItem.setText("About");
			aboutMenuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JDialog aboutDialog = getAboutDialog();
					aboutDialog.pack();
					Point loc = getJFrame().getLocation();
					loc.translate(20, 20);
					aboutDialog.setLocation(loc);
					aboutDialog.setVisible(true);
				}
			});
		}
		return aboutMenuItem;
	}

	/**
	 * This method initializes aboutDialog	
	 * 	
	 * @return javax.swing.JDialog
	 */
	private JDialog getAboutDialog() {
		if (aboutDialog == null) {
			aboutDialog = new JDialog(getJFrame(), true);
			aboutDialog.setTitle("About");
			aboutDialog.setContentPane(getAboutContentPane());
		}
		return aboutDialog;
	}

	/**
	 * This method initializes aboutContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getAboutContentPane() {
		if (aboutContentPane == null) {
			aboutContentPane = new JPanel();
			aboutContentPane.setLayout(new BorderLayout());
			aboutContentPane.add(getAboutVersionLabel(), BorderLayout.CENTER);
		}
		return aboutContentPane;
	}

	/**
	 * This method initializes aboutVersionLabel	
	 * 	
	 * @return javax.swing.JLabel	
	 */
	private JLabel getAboutVersionLabel() {
		if (aboutVersionLabel == null) {
			aboutVersionLabel = new JLabel();
			aboutVersionLabel.setText("bwidawsk");
			aboutVersionLabel.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return aboutVersionLabel;
	}

	/**
	 * This method initializes jMenuItem	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getViewMovesMenuItem() {
		if (viewMovesMenuItem == null) {
			viewMovesMenuItem = new JMenuItem();
			viewMovesMenuItem.setText("Moves");
			//viewMovesMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,
			viewMovesMenuItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					MoveStack moves;
					synchronized(GuiMain.actionOccured) {
						GuiMain.actionOccured.cmd = new CommandGetMoves();
						GuiMain.actionOccured.expectsReturn = true;
						GuiMain.actionOccured.notify();
						try {
							GuiMain.actionOccured.wait();
						} catch (InterruptedException ie) {
							ie.printStackTrace();
						}
						moves = (MoveStack)GuiMain.actionOccured.returnVal;
					}
					MovePanel mp = new MovePanel(moves);
					MoveDialog md = new MoveDialog(getJFrame());
					md.setTextBox(mp);
					md.setModalityType(ModalityType.APPLICATION_MODAL);
					md.setVisible(true);
					//PopupFactory.getSharedInstance().getPopup(getJFrame(), mp, 0, 0).show();
					//JOptionPane.showMessageDialog(getJFrame(), moves);
					
				}
			});
				//	Event.CTRL_MASK, true));
		}
		return viewMovesMenuItem;
	}
	/**
	 * This method initializes jMenuItem	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getUndoMenuItem() {
		if (undoMenuItem == null) {
			undoMenuItem = new JMenuItem();
			undoMenuItem.setText("Undo");
			undoMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z,
					Event.CTRL_MASK, true));
			undoMenuItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					synchronized(GuiMain.actionOccured) {
						GuiMain.actionOccured.cmd = new CommandUndo(board);
						GuiMain.actionOccured.expectsReturn = false;
						GuiMain.actionOccured.notify();
					}
					GuiMain.this.moveUpdate(null);
				}
			}
			);
		}
		return undoMenuItem;
	}
	/**
	 * This method initializes jMenuItem	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getSaveMenuItem() {
		if (saveMenuItem == null) {
			saveMenuItem = new JMenuItem();
			saveMenuItem.setText("Save");
			saveMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
					Event.CTRL_MASK, true));
		}
		return saveMenuItem;
	}
	
	
	
	public void externalMain() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				GuiMain.this.getJFrame().setVisible(true);
			}
		});
	}

	public static GuiAction actionOccured = new GuiAction();
	private JMenuItem scoreMenuItem = null;
	
	public void moveUpdate(Move m) {
		// do some animation with m's moves
		this.getJFrame().repaint();
	}
	@Override
	public InputCommand getCommand(Game g) {
		return actionOccured.cmd;
	}
	
	public void popupError(String message) {
		JOptionPane.showMessageDialog(this.getJFrame(), message, "Bengo Error", JOptionPane.ERROR_MESSAGE);
	}
	/**
	 * This method initializes scoreMenuItem	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getScoreMenuItem() {
		if (scoreMenuItem == null) {
			scoreMenuItem = new JMenuItem();
			scoreMenuItem.setEnabled(true);
			scoreMenuItem.setText("Score");
			scoreMenuItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					MoveStack moves;
					synchronized(GuiMain.actionOccured) {
						GuiMain.actionOccured.cmd = new CommandGetScore();
						GuiMain.actionOccured.expectsReturn = true;
						GuiMain.actionOccured.notify();
						try {
							GuiMain.actionOccured.wait();
						} catch (InterruptedException ie) {
							ie.printStackTrace();
						}
						Score score1 = (Score) GuiMain.actionOccured.returnVal;
						int white = score1.getWhiteScore();
						int black = score1.getBlackScore();
						System.out.println("Area score:");
						System.out.println("\twhite:\t" + white);
						System.out.println("\tblack:\t" + black);
						Score score2 = (Score) GuiMain.actionOccured.returnValAux;
						white = score2.getWhiteScore();
						black = score2.getBlackScore();
						System.out.println("Territory score:");
						System.out.println("\twhite:\t" + white);
						System.out.println("\tblack:\t" + black);
						AreaScore aScore = (AreaScore) score1;
						JDialog diag = new JDialog();
						diag.setContentPane(new BoardPanel(aScore.getModifiedBoard()));
						diag.setTitle("Area Score");
						diag.setModalityType(ModalityType.APPLICATION_MODAL);
						diag.setMinimumSize(new Dimension(100,100));
						diag.setVisible(true);
					}

				}
			});
		}
		return scoreMenuItem;
	}
	private void replaceBoardPanel(Board board) {
		this.getJFrame().setContentPane(new BoardPanel(board));
		this.getJFrame().repaint();
	}
}
