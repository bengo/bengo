package graphics;

import input.InputCommand;

public class GuiAction {
	public int x;
	public int y;
	public InputCommand cmd;
	public boolean expectsReturn;
	public Object returnVal;
	public Object returnValAux;
	
}
