package game;

import input.PlayerInput;

/**
 * Abstraction for a player. There are two players, black and white.
 * However in the way the logic is handled, we also have a neutral or
 * lack of player, and a player for the parameter of the board.
 * @author ben
 *
 */
public class Player {
	public static Player Black = new Player("black");
	public static Player EmptyOwner = new Player("clear");
	public static Player OffBoardOwner = new Player("off");
	public static Player White = new Player("white");
	
	public int capturedPieces;
	private String color;
	private int handicap;
	private PlayerInput input;

	private Player opponent;
	public Player(String color) {
		super();
		this.color = color;
		this.capturedPieces = 0;
		this.handicap = 0;
		this.input = null;
	}
	/**
	 * Add num captured pieces to the total this player has captured.
	 * @param num Number of pieces caputured.
	 */
	public void addCaptured(int num) {
		this.capturedPieces += num;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Player) {
			Player p = (Player) obj;
			return this.color.equals(p.color);
		}
		return false;
	}
	
	public int getHandicap() {
		return handicap;
	}
	
	public PlayerInput getInput() {
		return input;
	}
	
	public Player getOpponent() {
		return opponent;
	}
	
	public void setInput(PlayerInput input) {
		this.input = input;
	}
	
	public void setOpponent(Player opponent) {
		this.opponent = opponent;
	}
	
	@Override
	public String toString() {
		return color;
	}
}
