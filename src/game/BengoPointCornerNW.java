package game;

public class BengoPointCornerNW extends BengoPoint {

	public BengoPointCornerNW(int x, int y, Board parent) {
		super(x, y, parent);
	}

	@Override
	public void setAdjacents() {
		adjacentPoints.add(new BengoPointOffBoard(x - 1, y, board));
		adjacentPoints.add(getRightPoint());
		adjacentPoints.add(new BengoPointOffBoard(x,y - 1, board));
		adjacentPoints.add(getDownPoint());
	}
}
