package game;

import java.util.ArrayList;

public class BengoPointRightEdge extends BengoPoint {

	public BengoPointRightEdge(int x, int y, Board parent) {
		super(x, y, parent);
		adjacentPoints = new ArrayList<BengoPoint>(3);

	}

	@Override
	public void setAdjacents() {
		adjacentPoints.add(getLeftPoint());
		adjacentPoints.add(new BengoPointOffBoard(x + 1, y, board));
		adjacentPoints.add(getUpPoint());
		adjacentPoints.add(getDownPoint());
	}
}
