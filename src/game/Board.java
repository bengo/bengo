package game;

import java.util.ArrayList;

import other.BoardCoordinate;
import other.GameOptions;
import other.Move;

import exceptions.AlreadyOccupiedException;
import exceptions.SuicideException;

public class Board {
	private int size;
	private BengoPoint[][] points;
	private ArrayList<Connection> connections = new ArrayList<Connection>();
	private ArrayList<Connection> emptyConnections = new ArrayList<Connection>();
	
	public Board(int boardDimension) {
		size = boardDimension;
		points = new BengoPoint[size][size];
		Connection c = new Connection(this, Player.EmptyOwner);
		for(int i = 0; i < size; i++)
			for(int j = 0; j < size; j++)
				c.add(getPoint(i, j).toBoardCoordinate());
		
		emptyConnections.add(c);
	}

	public ArrayList<BengoPoint> bcToBp(ArrayList<BoardCoordinate> coordinates) {
		ArrayList<BengoPoint> returnPts = new ArrayList<BengoPoint>(coordinates.size());
		for(BoardCoordinate bc : coordinates) {
			returnPts.add(getPoint(bc.getX(), bc.getY()));
		}
		
		return returnPts;
	}
	
	private BengoPoint createPoint(int x, int y) {
		BengoPoint b;
		if (x == 0 && y == 0) {
			b = new BengoPointCornerNW(x, y, this);
		} else if (x == 0 && y == size - 1) {
			b = new BengoPointCornerSW(x, y, this);
		} else if (x == size - 1 && y == 0) {
			b = new BengoPointCornerNE(x, y, this);
		} else if (x == size - 1&& y == size - 1) {
			b = new BengoPointCornerSE(x, y, this);
		} else if (x == 0) {
			b = new BengoPointLeftEdge(x, y, this);
		} else if (x == size - 1) {
			b = new BengoPointRightEdge(x, y, this);
		} else if (y == 0) {
			b = new BengoPointTopEdge(x, y, this);
		} else if (y == size - 1) {
			b = new BengoPointBottomEdge(x, y, this);
		} else {
			b = new BengoPointRegular(x, y, this);
		}
		points[x][y] = b;
		return b;
	}
	
	public int getSize() {
		return size;
	}
	
	public BengoPoint getPoint(int x, int y) {
		if (points[x][y] == null) {
			createPoint(x,y).setAdjacents();
		}
		return points[x][y];
	}
	
	public BengoPoint getPoint(BoardCoordinate bc) {
		if (points[bc.getX()][bc.getY()] == null) {
			createPoint(bc.getX(),bc.getY()).setAdjacents();
		}
		return points[bc.getX()][bc.getY()];
	}
	
	public ArrayList<Connection> getConnections() {
		return connections;
	}

	private void placeConnection(Player p, Connection c) {
		for(BoardCoordinate b : c) {
			this.getPoint(b).occupy(p);
			connectFriends(getPoint(b));
		}
	}
	
	private void removeConnection(Connection c) {
		c.destroy();
		connections.remove(c);
	}
	
	public void undoMove(Move m) {
		removeConnection(m.getNewConnection());
		for(Connection c : m.getOldConnections()) {
			placeConnection(m.getPlayer(), c);	
		}
		for(Connection c : m.getCapturedConnection()) {
			placeConnection(m.getPlayer().getOpponent(), c);
		}
		this.emptyConnections = m.getOldEmptyConnections();
	}
	
	private ArrayList<Connection> removeEmptylistWithPoint(final BengoPoint point) {
		int found = 0;
		ArrayList<Connection> newCons = (ArrayList<Connection>) emptyConnections.clone();
		for(Connection c : emptyConnections) {
			if(c.contains(point)) {
				found++;
				//c.remove(point);
				//c.destroy();
				newCons.remove(c);
				break;
			}
		}
		
		assert found < 2 ;
		emptyConnections = newCons;
		return emptyConnections;
	}
	
	
	
	/**
	 * Rebuild all the empty connections on the board around the
	 * move at point. 
	 * This could turn 1 emptyConnection into 4
	 * TODO: This is very poor performance.
	 * @param point
	 * @return
	 */
	private void rebuildEmptyConnections(BengoPoint point) {
		ArrayList<BengoPoint> points = new ArrayList<BengoPoint>();
		ArrayList<BengoPoint> tempPoints = point.getAdjacent(null);
		for(BengoPoint p : tempPoints) {
			if(!(p instanceof BengoPointOffBoard)) {
				points.add(p);
			}
		}
		assert points.size() != 0 ;
		ArrayList<Connection> newCons = new ArrayList<Connection>();
		int i = 0;
		Connection c = null;
		for(i = 0; i < points.size(); i++) {
			if(points.get(i).getOwner().equals(Player.EmptyOwner)) {
				c = Connection.buildConnectionFromPoint(points.get(i));
				break;
			}
		}
		if (c == null) {
			return;
		}
		
		newCons.add(c);
		for(i = 1; i < points.size(); i++) {
			if(!points.get(i).getOwner().equals(Player.EmptyOwner))
				continue;
			c = Connection.buildConnectionFromPoint(points.get(i));
			boolean matched = false;
			for(int j = 0; j < newCons.size(); j++) {
				if(c.equals(newCons.get(j))) {
					matched = true;
				}
			}
			if(!matched)
				newCons.add(c);
		}
		
		emptyConnections.addAll(newCons);
	}
	
	public boolean isSuicide(Move move) {
		Connection c = new Connection(this, move.getPlayer());
		c.addAll(move.getMoveCoord().getAdjacentConnections(move.getPlayer()));
		c.add(move.getMoveCoord().toBoardCoordinate());
		if(c.getLiberties().size() == 0) {
			return true;
		}
		return false;
	}
	
	public Move makeMove(Move m) throws AlreadyOccupiedException, SuicideException {
		Move retMove = new Move(m.getMoveNo());
		retMove.setMoveCoord(m.getMoveCoord());
		BengoPoint p = m.getMoveCoord();
		
		if (p.isOccupied()) {
			throw new AlreadyOccupiedException();
		}
				
		p.occupy(m.getPlayer());
		/* It's always safe to merge existing connections first??? */
		Move tempMove = connectFriends(p);
		retMove.setPlayer(m.getPlayer());
		retMove.setOldConnections(tempMove.getOldConnections());
		retMove.setNewConnection(tempMove.getNewConnection());
		retMove.setCapturedConnection(killEnemies(p));
		// We must check for suicide after potential captures
		if(!GameOptions.getInstance().isSuicideAllowed() && isSuicide(m)) {
			p.unoccupy();
			throw new SuicideException();
		}
		retMove.setBs(this.getBoardState());
		retMove.setOldEmptyConnections(emptyConnections);
		removeEmptylistWithPoint(p);
		rebuildEmptyConnections(p);
		return retMove;
	}

	private ArrayList<Connection> killEnemies(BengoPoint p) {
		ArrayList<Connection> retCons = new ArrayList<Connection>();
		ArrayList<Connection> cons;
		cons = p.getAdjacentEnemyConnections();
		for (Connection c : cons) {
			if(c.getLiberties().size() == 0) {
				retCons.add(c);
				c.destroy();
				connections.remove(c);
			}
		}
		return retCons;
	}
	
	// mnew connection, and old connections are set after this is done
	private Move connectFriends(BengoPoint p) {
		Move m = new Move();
		ArrayList<Connection> retCons = new ArrayList<Connection>();
		
		// Create a new connection for the point that was placed.
		// In the simple case we'll be done after this.
		Connection c = new Connection(this, p.getOwner());
		c.connect(p);
		connections.add(c); // add to the list of connections
		
		ArrayList<Connection> cons;
		cons = p.getAdjacentFriendlyConnections();

		// get all the friendly connections and merge the new connection to it
		if (cons.size() > 0) {
			Connection cn = new Connection(this, p.getOwner());
			for (Connection co : cons) {
				cn.connect(co);
				retCons.add(co);
				connections.remove(co);
			}
			/* we're attaching to a connection so remove our new connection */
			cn.connect(c);
			connections.remove(c);
			
			/* add the mega connection */
			connections.add(cn);
			m.setNewConnection(cn);
		} else {
			m.setNewConnection(c);
		}
		
		m.setOldConnections(retCons);
		return m;
	}
	
	public BoardState getBoardState() {
		BoardState retState = new BoardState(size);
		Player[][] ret = new Player[size][size];
		int x, y;
		for(y = 0; y < size; y++) {
			for(x = 0; x < size; x++) {
				ret[x][y] = this.getPoint(x, y).getOwner();
			}
		}
		retState.setState(ret);
		return retState;
	}

	public void showConnections() {
		for (Connection c : getConnections())
			System.out.println(c);
	}
	private Player whoSurrounds(Connection c) {
		Player last = null;
		int count = 0;
		for(BengoPoint point : c.getNotLiberties()) {
			if(last == null) {
				last = point.getOwner();
			} else {
				if (!last.equals(point.getOwner())) {
					return null;
				}
			}
			count++;
		}
		if(count > 1)
			return last;
			
		return null;
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		Board board = new Board(this.size);
		board.connections = (ArrayList<Connection>) connections.clone();
		
		for(int i = 0; i < size; i++) {
			for(int j = 0; j < size; j++) {
				BengoPoint p = board.getPoint(i, j);
				p.setBoard(board);
				for(Connection c : board.connections) {
					if(c.contains(p)) {
						p.setConnection(c);
						break;
					}
				}
			}
		}
		
		//board.points = points.clone();
		board.emptyConnections = (ArrayList<Connection>) emptyConnections.clone();
		return board;
		
	}

	public Board fakeFillBoard() {
		Board tempBoard;
		try {
			tempBoard = (Board) this.clone();
			//System.out.println(tempBoard.connections);
			tempBoard.fillBoard();
			return tempBoard;
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Inefficient
	 * @return
	 */
	public BoardState fillBoard() {
		Move m = new Move();
		ArrayList<Connection> tempCons = (ArrayList<Connection>) emptyConnections.clone();
		for(Connection emptyConnection : tempCons) {
			Player boss = whoSurrounds(emptyConnection);
			if(boss == null)
				continue;
			
			// the empty connection may be surrounded by one or
			// more connections, 
			//we'll need old connections
			// new connection
			// old empty
			// boardstate
			//captured connections will be null
			
			for(BoardCoordinate b : emptyConnection) {
				BengoPoint p = getPoint(b);
				p.occupy(boss);
				connectFriends(p);
				removeEmptylistWithPoint(p);
				rebuildEmptyConnections(p);
			}
		}
		return this.getBoardState();
	}
}
//
//Move tempMove = connectFriends(p);
//retMove.setPlayer(m.getPlayer());
//retMove.setOldConnections(tempMove.getOldConnections());
//retMove.setNewConnection(tempMove.getNewConnection());
//retMove.setCapturedConnection(killEnemies(p));
//// We must check for suicide after potential captures
//if(!GameOptions.getInstance().isSuicideAllowed() && isSuicide(m)) {
//	p.unoccupy();
//	throw new SuicideException();
//}
//retMove.setBs(this.getBoardState());
//retMove.setOldEmptyConnections(emptyConnections);
//removePointFromEmptyList(p);
//rebuildEmptyConnections(p);
