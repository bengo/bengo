package game;

public class BoardState {
	Player[][] state;
	int size;
	int white = 0;
	public int getWhite() {
		return white;
	}

	public int getBlack() {
		return black;
	}

	int black = 0;
	int neutral = 0;
	public BoardState(int size) {
		super();
		this.size = size;
		//state = new Player[size][size];
	}

	public void setState(Player[][] state) {
		this.state = state;
		for(int i = 0; i < size; i++) {
			for(int j = 0; j < size; j++) {
				if (state[i][j].equals(Player.White))
					white++;
				else if (state[i][j].equals(Player.Black))
					black++;
				else
					neutral++;
			}
		}
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof BoardState) {
			BoardState bs = (BoardState)obj;
			int x = 0;
			int y = 0;
			for(y = 0; y < size; y++) {
				for(x = 0; x < size; x++) {
					if(this.state[x][y] == null && bs.state[x][y] == null)
						continue;
					
					if(this.state[x][y] == null && bs.state[x][y] != null)
						return false;
					
					if(!this.state[x][y].equals(bs.state[x][y]))
						return false;
				}
			}
			return true;
		}
		
		return false;
	}

	@Override
	public String toString() {
		String retString = "black: " + black + "\n";
		retString += "white: " + white + "\n";
		retString += "neutral: " + neutral;
		return retString;
	}

}
