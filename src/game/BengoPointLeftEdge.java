package game;

public class BengoPointLeftEdge extends BengoPoint {

	public BengoPointLeftEdge(int x, int y, Board parent) {
		super(x, y, parent);
	}

	@Override
	public void setAdjacents() {
		adjacentPoints.add(new BengoPointOffBoard(x - 1, y, board));
		adjacentPoints.add(getRightPoint());
		adjacentPoints.add(getUpPoint());
		adjacentPoints.add(getDownPoint());
	}	
}
