package game;

public class BengoPointCornerNE extends BengoPoint {

	public BengoPointCornerNE(int x, int y, Board parent) {
		super(x, y, parent);
	}

	@Override
	public void setAdjacents() {
		adjacentPoints.add(getLeftPoint());
		adjacentPoints.add(new BengoPointOffBoard(x + 1, y, board));
		adjacentPoints.add(new BengoPointOffBoard(x, y - 1, board));
		adjacentPoints.add(getDownPoint());
	}
}
