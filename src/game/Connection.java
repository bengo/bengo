package game;

import java.util.ArrayList;
import java.util.HashSet;

import other.BoardCoordinate;

/**
 * A connection is a unique set of points who are adjacent to one another.
 * @author ben
 *
 */
public class Connection extends HashSet<BoardCoordinate> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3436060117929076203L;
	
	/**
	 * Given a point, build a connection with the point, and return that connection.
	 * @param point he point to build the connection from
	 * @return connection built containing the point
	 */
	public static Connection buildConnectionFromPoint(BengoPoint point) {
		return buildConnectionFromPoint(point, null);
	}
	

//	private static Connection buildConnectionFromPoint(BengoPoint point, Connection connection) {
//		if (connection == null) {
//			connection = new Connection(point.board, point.getOwner());
//			connection.add(point.toBoardCoordinate());
//		} 
//		
//		BengoPoint temp;
//		temp = point.getLeftPoint();
//		
//		// Each point works the same.
//		// 1. It has the same ownership as the originating point.
//		// 2. The final connection does not already contain this point (prevent cycles)
//		// 3. The point is actually on the board.
//		if (point.getOwner().equals(temp.getOwner()) && !connection.contains(temp) && BengoPoint.isOnBoard(point)) {
//			connection.add(temp.toBoardCoordinate());
//			connection.addAll(buildConnectionFromPoint(temp, connection));
//		}
//		
//		temp = point.getRightPoint();
//		
//		if(point.getOwner().equals(temp.getOwner()) && !connection.contains(temp) && BengoPoint.isOnBoard(point)) {
//			connection.add(temp.toBoardCoordinate());
//			connection.addAll(buildConnectionFromPoint(temp, connection));
//		}
//		temp = point.getUpPoint();
//		
//		if(point.getOwner().equals(temp.getOwner()) && !connection.contains(temp) && BengoPoint.isOnBoard(point)) {
//			connection.add(temp.toBoardCoordinate());
//			connection.addAll(buildConnectionFromPoint(temp, connection));
//		}
//		temp = point.getDownPoint();
//		
//		if(point.getOwner().equals(temp.getOwner()) && !connection.contains(temp) && BengoPoint.isOnBoard(point)) {
//			connection.add(temp.toBoardCoordinate());
//			connection.addAll(buildConnectionFromPoint(temp, connection));
//		}
//		
//		return connection;
//	}
	
	/**
	 * Worker function for the static {@link #buildConnectionFromPoint(BengoPoint) 
	 * buildConnectionFromPoint} method.
	 * 
	 * The strategy is to recursive follow each adjacent point's chain until we build
	 * up the full list. The order, left, right, up, down is completely arbitrary..
	 * @param point Point to build the connection from.
	 * @param connection Connection passed to recursive calls
	 * @return connection built from the point passed in
	 * TODO: previously I had the code above, but it seemed wrong. Is the following correct?
	 * 		The different is checking the stone is on board, before it was point, now it is temp
//	 */
	private static Connection buildConnectionFromPoint(BengoPoint point, Connection connection) {
		if (connection == null) {
			connection = new Connection(point.board, point.getOwner());
			connection.add(point.toBoardCoordinate());
		} 
		
		BengoPoint temp;
		temp = point.getLeftPoint();
		
		// Each point works the same.
		// 1. It has the same ownership as the originating point.
		// 2. The final connection does not already contain this point (prevent cycles)
		// 3. The point is actually on the board.
		if (point.getOwner().equals(temp.getOwner()) && !connection.contains(temp) && BengoPoint.isOnBoard(temp)) {
			connection.add(temp.toBoardCoordinate());
			connection.addAll(buildConnectionFromPoint(temp, connection));
		}
		
		temp = point.getRightPoint();
		
		if(point.getOwner().equals(temp.getOwner()) && !connection.contains(temp) && BengoPoint.isOnBoard(temp)) {
			connection.add(temp.toBoardCoordinate());
			connection.addAll(buildConnectionFromPoint(temp, connection));
		}
		temp = point.getUpPoint();
		
		if(point.getOwner().equals(temp.getOwner()) && !connection.contains(temp) && BengoPoint.isOnBoard(temp)) {
			connection.add(temp.toBoardCoordinate());
			connection.addAll(buildConnectionFromPoint(temp, connection));
		}
		temp = point.getDownPoint();
		
		if(point.getOwner().equals(temp.getOwner()) && !connection.contains(temp) && BengoPoint.isOnBoard(temp)) {
			connection.add(temp.toBoardCoordinate());
			connection.addAll(buildConnectionFromPoint(temp, connection));
		}
		
		return connection;
	}
	private Player owner;
	
	private Board board;

	public Connection(Board board, Player p) {
		super();
		this.board = board;
		this.owner = p;
	}

	public void addAll(ArrayList<Connection> connections) {
		for(Connection c : connections) {
			for(BoardCoordinate bp : c) {
				this.add(bp);
			}
		}
	}
	
	public void connect(BengoPoint p) {
		this.add(p.toBoardCoordinate());
		p.setConnection(this);
	}
	
	public void connect (Connection c) {
		for (BoardCoordinate b : c) {
			this.add(b);
			board.getPoint(b).setConnection(this);
		}
	}
	
	@Override
	public boolean contains(Object o) {
		if(o instanceof BoardCoordinate) {
			BoardCoordinate boco = (BoardCoordinate) o;
			for(BoardCoordinate bc : this) {
				if(bc.equals(boco)) {
					return true;
				}
			}
		} else if (o instanceof BengoPoint){
			BengoPoint boco = (BengoPoint) o;
			for(BoardCoordinate bc : this) {
				if(bc.equals(boco.toBoardCoordinate())) {
					return true;
				}
			}
		}
		return false;
	}
	
	public void destroy() {
		for (BoardCoordinate b : this)
			board.getPoint(b).destroy();
	}

	private ArrayList<BengoPoint> getAdjacentPoints() {
		ArrayList<BengoPoint> retPts = new ArrayList<BengoPoint>();
		for(BoardCoordinate bp : this) {
			for(BengoPoint p : board.getPoint(bp).adjacentPoints) {
				if(p != null && !contains(p) && (!(p instanceof BengoPointOffBoard)))
					retPts.add(p);
			}
		}
		return retPts;
	}

	public ArrayList<BengoPoint> getLiberties() {
		ArrayList<BengoPoint> retList = new ArrayList<BengoPoint>();
		for(BengoPoint p : getAdjacentPoints()) {
			if (p.getOwner().equals(Player.EmptyOwner))
				retList.add(p);
		}
		
		return retList;
	}

	public ArrayList<BengoPoint> getNotLiberties() {
		ArrayList<BengoPoint> retList = new ArrayList<BengoPoint>();
		HashSet<BengoPoint> tempSet = new HashSet<BengoPoint>();
		for(BengoPoint p : getAdjacentPoints()) {
			tempSet.add(p);
		}
		for(BengoPoint p : tempSet) {
			if(p instanceof BengoPointOffBoard)
				continue;
			
			if (!p.getOwner().equals(Player.EmptyOwner))
				retList.add(p);
		}
		
		return retList;
	}
	
	public Player getOwner() {
		return owner;
	}

	public boolean isSurrounded() {
		return (this.getLiberties().size() > 0);
	}

	@Override
	public String toString() {
		String retString = new String();
		for (BoardCoordinate b : this)
			retString += b.toString() + " ";

		return retString;
	}
	
	
}
