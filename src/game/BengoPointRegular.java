package game;

public class BengoPointRegular extends BengoPoint {

	public BengoPointRegular(int x, int y, Board parent) {
		super(x, y, parent);
	}

	@Override
	public void setAdjacents() {
		adjacentPoints.add(getLeftPoint());
		adjacentPoints.add(getRightPoint());
		adjacentPoints.add(getUpPoint());
		adjacentPoints.add(getDownPoint());
	}
}
