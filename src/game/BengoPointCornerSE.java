package game;

public class BengoPointCornerSE extends BengoPoint {

	public BengoPointCornerSE(int x, int y, Board parent) {
		super(x, y, parent);
	}

	@Override
	public void setAdjacents() {
		adjacentPoints.add(getLeftPoint());
		adjacentPoints.add(new BengoPointOffBoard(x + 1, y, board));
		adjacentPoints.add(getUpPoint());
		adjacentPoints.add(new BengoPointOffBoard(x, y + 1, board));
		
	}
}
