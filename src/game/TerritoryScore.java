package game;

/**
 * Implements territory scoring:
 * {@link http://en.wikipedia.org/wiki/Rules_of_go#Territory_scoring}
 * In short: (territory surrounded) - (stones captured)
 * @author ben
 *
 */

public class TerritoryScore implements Score {

	private Player white;
	private Player black;
	private Board board;
	
	
	public TerritoryScore(Player white, Player black, Board board) {
		super();
		this.white = white;
		this.black = black;
		this.board = board;
	}

	@Override
	public int getBlackScore() {
		// First get the number of white and black to subtract later
		BoardState bs = board.getBoardState();
		int black_current = bs.getBlack();
		
		// Do a fake fill which creates a board with territory filled in.
		Board b = board.fakeFillBoard();
		
		// territory does not include areas which are occupied by stones
		int black_ter = b.getBoardState().getBlack() - black_current;
		
		int black_score = black_ter - white.capturedPieces;
		
		return black_score;
	}

	@Override
	public int getWhiteScore() {
		// First get the number of white and black to subtract later
		BoardState bs = board.getBoardState();
		int white_current = bs.getWhite();
		
		// Do a fake fill which creates a board with territory filled in.
		Board b = board.fakeFillBoard();
		
		// territory does not include areas which are occupied by stones
		int white_ter = b.getBoardState().getWhite() - white_current;
		
		int white_score = white_ter - black.capturedPieces;
		
		return white_score;
	}

}
