package game;

/**
 * There are at least two completely distinct scoring methods (and small variations within each.
 * Any class implementing this interface must be able to calculate a black score, and a white score.
 * @author ben
 *
 */
public interface Score {
	public int getWhiteScore();
	public int getBlackScore();
}
