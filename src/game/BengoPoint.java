package game;


import java.util.ArrayList;

import other.BoardCoordinate;

public abstract class BengoPoint {
	protected int x;
	protected int y;

	protected Board board;
	private Connection connection;

	private Player owner;
	protected ArrayList<BengoPoint> adjacentPoints = new ArrayList<BengoPoint>(4);
	
	public static boolean isOnBoard(BengoPoint p) {
		return !(p instanceof BengoPointOffBoard);
	}
	
	public BengoPoint(int x, int y, Board board) {
		this(x,y,board, Player.EmptyOwner);
	}
	
	public BengoPoint(int x, int y, Board board, Player p) {
		this.x = x;
		this.y = y;
		this.board = board;
		this.owner = p;
	}
	
	public void destroy() {
		this.connection = null;
		this.owner = Player.EmptyOwner;
	}
	
	/**
	 * Warning only use this if you know what you're doing. This used to be private.
	 * @param targetPlayer
	 * @return
	 */
	public ArrayList<BengoPoint> getAdjacent(Player targetPlayer) {
		ArrayList<BengoPoint> bp = new ArrayList<BengoPoint>();
		for(BengoPoint b : adjacentPoints) {
			if(targetPlayer != null) {
				if (b.isOccupied() && targetPlayer.equals(b.getOwner()))
					bp.add(b);	
			} else {
				bp.add(b);
			}
		}
		return bp;
	}
	
	public ArrayList<Connection> getAdjacentConnections(Player targetPlayer) {
		ArrayList<Connection> retCon = new ArrayList<Connection>();
		for(BengoPoint p : getAdjacent(targetPlayer)) {
			if(!retCon.contains(p.getConnection())) {
				retCon.add(p.getConnection());
			}
		}
		return retCon;
	}
	
	public ArrayList<Connection> getAdjacentEnemyConnections() {
		ArrayList<Connection> retCon = new ArrayList<Connection>();
		for(BengoPoint p : getAdjacent(getOwner().getOpponent())) {
			if(!retCon.contains(p.getConnection())) {
				retCon.add(p.getConnection());
			}
		}
		return retCon;
	}
	public ArrayList<Connection> getAdjacentFriendlyConnections() {
		ArrayList<Connection> retCon = new ArrayList<Connection>();
		for(BengoPoint p : getAdjacent(getOwner())) {
			if(!retCon.contains(p.getConnection())) {
				retCon.add(p.getConnection());
			}
		}
		return retCon;
	}
	
	public Connection getConnection() {
		return connection;
	}

	public BengoPoint getDownPoint() {
		if(adjacentPoints.size() == 3)
			return this.board.getPoint(x, y + 1);
		return adjacentPoints.get(3);
	}

	public BengoPoint getLeftPoint() {
		if(adjacentPoints.size() == 0)
			return this.board.getPoint(x - 1, y);
		return adjacentPoints.get(0);
	}

	ArrayList<BengoPoint> getLiberties() {
		ArrayList<BengoPoint> bp = new ArrayList<BengoPoint>();
		for(BengoPoint b : adjacentPoints) {
			if (!b.isOccupied())
				bp.add(b);
		}
		return bp;
	}

	public Player getOwner() {
		return owner;
	}
	
	public BengoPoint getRightPoint() {
		if(adjacentPoints.size() == 1)
			return this.board.getPoint(x + 1, y);
		return adjacentPoints.get(1);
	}
	
	private int getTotalAdjacentPoints() {
		return adjacentPoints.size();
	}

	public BengoPoint getUpPoint() {
		if(adjacentPoints.size() == 2)
			return this.board.getPoint(x, y - 1);
		return adjacentPoints.get(2);
	}
	public boolean isLibertiesMax() {
		int x = getLiberties().size();
		int y = getTotalAdjacentPoints();
		return x == y;
	}
	
	public boolean isOccupied() {
		return !this.owner.equals(Player.EmptyOwner) && !this.owner.equals(Player.OffBoardOwner);
	}
	public void occupy(Player who) {
		this.owner = who;
	}

	public abstract void setAdjacents();
	
	public void setBoard(Board board) {
		this.board = board;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
		this.owner = connection.getOwner();
	}

	public boolean surroundedByFriends() {
		return getAdjacentFriendlyConnections().size() == getTotalAdjacentPoints();
	}
	
	public BoardCoordinate toBoardCoordinate() {
		return new BoardCoordinate(this.x, this.y);
	}
	
	@Override
	public String toString() {
		return new String(x + "," + y);
	}
	
	public void unoccupy() {
		this.owner = Player.EmptyOwner;
	}
	
}
