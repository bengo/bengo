package game;

public class BengoPointBottomEdge extends BengoPoint {

	public BengoPointBottomEdge(int x, int y, Board parent) {
		super(x, y, parent);
	}

	@Override
	public void setAdjacents() {	
		adjacentPoints.add(this.getLeftPoint());
		adjacentPoints.add(this.getRightPoint());
		adjacentPoints.add(this.getUpPoint());
		adjacentPoints.add(new BengoPointOffBoard(x, y + 1, board));
	}
}
