package game;

import graphics.GuiMain;

public class AreaScore implements Score {

	private Board board;
	
	
	
	public AreaScore(Player white, Player black, Board board) {
		super();
		this.board = board;
	}

	@Override
	public int getBlackScore() {
		Board b = board.fakeFillBoard();
		return b.getBoardState().getBlack();
	}

	@Override
	public int getWhiteScore() {
		Board b = board.fakeFillBoard();
		return b.getBoardState().getWhite();
	}

	public Board getModifiedBoard() {
		return board.fakeFillBoard();
	}
}
