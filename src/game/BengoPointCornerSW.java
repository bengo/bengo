package game;

public class BengoPointCornerSW extends BengoPoint {

	public BengoPointCornerSW(int x, int y, Board parent) {
		super(x, y, parent);

	}

	@Override
	public void setAdjacents() {
		adjacentPoints.add(new BengoPointOffBoard(-1, y, board));
		adjacentPoints.add(getRightPoint());
		adjacentPoints.add(getUpPoint());
		adjacentPoints.add(new BengoPointOffBoard(x, y + 1, board));
		
	}
}
