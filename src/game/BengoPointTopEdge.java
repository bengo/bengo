package game;

import java.util.ArrayList;

public class BengoPointTopEdge extends BengoPoint {

	public BengoPointTopEdge(int x, int y, Board parent) {
		super(x, y, parent);
		adjacentPoints = new ArrayList<BengoPoint>(3);

	}

	@Override
	public void setAdjacents() {
		adjacentPoints.add(getLeftPoint());
		adjacentPoints.add(getRightPoint());
		adjacentPoints.add(new BengoPointOffBoard(x, -1, board));
		adjacentPoints.add(getDownPoint());
	}	
}
