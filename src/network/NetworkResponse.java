package network;

import java.io.Serializable;

public class NetworkResponse implements Serializable {
	private boolean success;
	private String message;
	private NetworkCommand command;

	public void setCommand(NetworkCommand command) {
		this.command = command;
	}
	public NetworkCommand getCommand() {
		return command;
	}
	public NetworkResponse(boolean success, String message) {
		super();
		this.success = success;
		this.message = message;
		command = null;
	}
	
	public NetworkResponse(boolean success, String message, NetworkCommand cmd) {
		this(success, message);
		command = cmd;
	}

	public boolean isSuccess() {
		return success;
	}
	
}
