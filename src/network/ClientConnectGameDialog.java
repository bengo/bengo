package network;

import input.CommandMove;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import other.BoardCoordinate;

import java.awt.GridBagConstraints;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.awt.Dimension;

public class ClientConnectGameDialog extends JDialog implements Runnable {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JScrollPane jScrollPane = null;
	private JTable playerTable = null;
	private JPanel bottomPanel = null;
	private JButton refreshButton = null;
	private JButton observeButton = null;

	private boolean autoRefresh = true;
	ClientGame client;
	private JButton joinButton = null;
	/**
	 * @param owner
	 */
	public ClientConnectGameDialog(Frame owner, ClientGame game) {
		super(owner);
		initialize();
		client = game;
		new Thread(this).start();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(451, 200);
		this.setContentPane(getJContentPane());
		
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJScrollPane(), BorderLayout.CENTER);
			jContentPane.add(getBottomPanel(), BorderLayout.SOUTH);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jScrollPane	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setViewportView(getPlayerTable());
		}
		return jScrollPane;
	}

	/**
	 * This method initializes playerTable	
	 * 	
	 * @return javax.swing.JTable	
	 */
	private JTable getPlayerTable() {
		if (playerTable == null) {
			Object names[] = {"Name", "Color"};
			DefaultTableModel dtm = new DefaultTableModel(names, 2);
			dtm.setValueAt("White", 0, 1);
			dtm.setValueAt("Black", 1, 1);
			playerTable = new JTable(dtm);
			playerTable.setShowGrid(false);
		}
		return playerTable;
	}

	/**
	 * This method initializes bottomPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getBottomPanel() {
		if (bottomPanel == null) {
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 2;
			gridBagConstraints.gridy = 0;
			bottomPanel = new JPanel();
			bottomPanel.setLayout(new GridBagLayout());
			bottomPanel.add(getRefreshButton(), new GridBagConstraints());
			bottomPanel.add(getObserveButton(), new GridBagConstraints());
			bottomPanel.add(getWhiteJoinButton(), gridBagConstraints);
		}
		return bottomPanel;
	}

	/**
	 * This method initializes refreshButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getRefreshButton() {
		if (refreshButton == null) {
			refreshButton = new JButton();
			refreshButton.setText("Refresh");
			refreshButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					refresh();
				}
			});
		}
		return refreshButton;
	}

	/**
	 * This method initializes joinButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getObserveButton() {
		if (observeButton == null) {
			observeButton = new JButton();
			observeButton.setEnabled(false);
			observeButton.setText("Observe");
		}
		return observeButton;
	}
	
	private void refresh() {
		ServerGetPlayerListCommand sglp = new ServerGetPlayerListCommand("players", null);
		NetworkResponse res;
		try {
			res = client.sendSynchronousCommand(sglp);
			assert (res instanceof PlayerListServerResponse);
			PlayerListServerResponse list = (PlayerListServerResponse) res;
			playerTable.getModel().setValueAt(list.whitePlayer, 0, 0);
			playerTable.getModel().setValueAt(list.blackPlayer, 1, 0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		while (autoRefresh) {
			try {
				Thread.sleep(200);
				refresh();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

	/**
	 * This method initializes whiteJoinButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getWhiteJoinButton() {
		if (joinButton == null) {
			joinButton = new JButton();
			joinButton.setText("Join");
			joinButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					String name = JOptionPane.showInputDialog("Enter your name");
					int which = playerTable.getSelectedRow();
					ServerConnectCommand scc = new ServerConnectCommand("connect", which == 0? "white" : "black", name);
					try {
						client.sendSynchronousCommand(scc);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
		}
		return joinButton;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
