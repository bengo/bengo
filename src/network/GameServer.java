package network;

import game.Board;
import game.Player;
import graphics.CommandGetter;
import input.InputCommand;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import other.Game;
import other.GameOptions;
import other.Move;

public class GameServer implements Runnable, Game {
	class NameSocketPair {
		Socket socket;
		String name;
		Player player;
		NameSocketPair opponent;
	}
	private ServerSocket serverSocket;
	private NameSocketPair white = new NameSocketPair();
	private NameSocketPair black = new NameSocketPair(); 
	private ArrayList<NameSocketPair> viewers;
	private final Semaphore modifyPlayerLock = new Semaphore(1, true);
	
	private Board board;
	public GameServer(GameOptions options) {
		super();
		try {
			serverSocket = new ServerSocket(0, 50);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(serverSocket.getLocalPort());
		new Thread(this).start();
		board = new Board(options.getBoardSize());
		// The server takes and sends commands to clients, but
		// we also need something to communicate with the game
	}

	@Override
	public void run() {
		try {
			/* Keep accepting connections and dispatch to a new command handler */
			while( true ) {
				Socket client = serverSocket.accept();
				new Thread(new ServerRxThread(this, client)).start();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean checkAndSetWhite(ServerRxThread rxThread, String name) {
		boolean retval = true;
		try {
			modifyPlayerLock.acquire();
			if (getWhite().socket != null)
				retval = false;
			else {
				this.white.socket = rxThread.getClient();
				this.white.name = name;
				this.white.player = new Player("white");
				this.white.player.setInput(rxThread);
				this.white.opponent = this.black;
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			retval = false;
		}
		
		modifyPlayerLock.release();
		return retval;
	}
	
	public boolean checkAndSetBlack(ServerRxThread rxThread, String name) {
		boolean retval = true;
		try {
			modifyPlayerLock.acquire();
			if (getBlack().socket != null)
				retval = false;
			else {
				this.black.socket = rxThread.getClient();
				this.black.name = name;
				this.black.player = new Player("black");
				this.black.player.setInput(rxThread);
				this.black.opponent = this.white;
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			retval = false;
		}
		
		modifyPlayerLock.release();
		return retval;
	}
	synchronized NameSocketPair getWhite() {
		return white;
	}

	synchronized NameSocketPair getBlack() {
		return black;
	}

	public synchronized ArrayList<NameSocketPair> getViewers() {
		return viewers;
	}

	public synchronized void setViewers(ArrayList<NameSocketPair> viewers) {
		this.viewers = viewers;
	}

	
	@Override
	public void play() {
		// TODO: this should essentially copy what is in LocalGame.play()
		while (true) {
			int i = 0;
			while(this.white.player == null || this.black.player == null);
			System.out.println("Turn " + i);
			Player player = this.white.player;
			Move m = player.getInput().getMove(this, player);
			m.incMoveNo();
			System.out.println(m.getMoveCoord());
			while(true);
		}
	}

	@Override
	public Board getBoard() {
		return board;
	}
}
