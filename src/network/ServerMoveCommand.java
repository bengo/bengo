package network;

import input.CommandMove;

public class ServerMoveCommand extends NetworkCommand {

	private CommandMove move;
	
	public CommandMove getMove() {
		return move;
	}

	public ServerMoveCommand(String commandDesc, CommandMove move) {
		super(commandDesc);
		this.move = move;
	}
	
	@Override
	public NetworkResponse action(GameServer server,
			ServerRxThread serverRxThread) throws Exception {
		return new NetworkResponse(true, "FIX THIS");
	}

}
