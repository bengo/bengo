package network;

import java.util.ArrayList;

public class PlayerListServerResponse extends NetworkResponse {
	
	String whitePlayer;
	String blackPlayer;
	ArrayList<String> observers;
	
	public PlayerListServerResponse(boolean success, String message, String white, String black, ArrayList<String> obs) {
		super(success, message);
		whitePlayer = white;
		blackPlayer = black;
		//TODO:
		observers = null;
	}

	
}
