package network;

import java.io.Serializable;

import exceptions.server.ColorAlreadyTakenException;

import input.InputCommand;

public abstract class NetworkCommand implements Serializable {
	private String commandDesc;
	
	public NetworkCommand(String commandDesc) {
		super();
		this.commandDesc = commandDesc;
	}
	
	public String getCommandDesc() {
		return commandDesc;
	}
	
	public abstract NetworkResponse action(GameServer server, ServerRxThread serverRxThread) throws Exception;
}
