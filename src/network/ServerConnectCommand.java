package network;


import exceptions.server.ColorAlreadyTakenException;
import input.InputCommand;

public class ServerConnectCommand extends NetworkCommand {
	private String requestedColor;
	private String name;

	public ServerConnectCommand(String commandDesc, String color, String name) {
		super(commandDesc);
		this.requestedColor = color;
		this.name = name;
	}

	@Override
	public NetworkResponse action(GameServer server, ServerRxThread serverRxThread) throws Exception {
		if (requestedColor.equalsIgnoreCase("white")) {
			System.out.println(requestedColor + " requested");
			return new NetworkResponse(server.checkAndSetWhite(serverRxThread, name), null);
		} else if (requestedColor.equalsIgnoreCase("black")) {
			return new NetworkResponse(server.checkAndSetBlack(serverRxThread, name), null);
		} else {
			throw new Exception("Unknown color");
		}
	}
}
