package network;

import input.InputCommand;


public class ServerGetPlayerListCommand extends NetworkCommand {

	public ServerGetPlayerListCommand(String commandDesc,
			InputCommand gameCommand) {
		super(commandDesc);
		// TODO Auto-generated constructor stub
	}

	@Override
	public NetworkResponse action(GameServer server, ServerRxThread serverRxThread) throws Exception {
		return new PlayerListServerResponse(true, null, server.getWhite().name, server.getBlack().name, null);
	}

}
