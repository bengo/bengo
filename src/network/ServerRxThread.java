package network;

import game.Player;
import graphics.CommandGetter;
import graphics.GuiMain;
import input.CommandMove;
import input.InputCommand;
import input.PlayerInput;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

import other.Game;
import other.Move;

/**
 * There is one instance of this class per connection. Its job is to take in commands and responses
 * and deliver them to the right place within the system.
 * @author ben
 *
 */
public class ServerRxThread implements Runnable, PlayerInput {

	class CommandWrapper {
		InputCommand inputCommand;
	}
	private Socket client;
	public Socket getClient() {
		return client;
	}

	private GameServer gameServer;
	private ObjectOutputStream out;
	private ObjectInputStream in;
	
	public ServerRxThread(GameServer gs, Socket client) throws IOException {
		super();
		this.client = client;
		this.gameServer = gs;
		out = new ObjectOutputStream(client.getOutputStream());
		in = new ObjectInputStream(client.getInputStream());
	}
	
	@Override
	public void run() {
		// TODO: add code to accept a response
		while (client.isConnected()) {
			NetworkCommand command = null;
			try {
				command = (NetworkCommand) in.readObject();
			} catch (SocketException se) {
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
			try {
				out.writeObject((handleServerCommand(command)));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/* We have if condition mainly for the assertion, we can just do a command.action()
	 * at some point
	 */
	private NetworkResponse handleServerCommand(NetworkCommand command) {
		String description = command.getCommandDesc();
		NetworkResponse ret = null;
		if ("status".equals(description)) {
			
		}

		if (description.startsWith("connect")) {
			assert (command instanceof ServerConnectCommand);
			try {
				ret = command.action(gameServer, this);
				ret.setCommand(command);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (description.startsWith("disconnect")) {
			
		}
		if (description.startsWith("observe")) {
			
		}
		if ("players".equals(description)) {
			assert (command instanceof ServerGetPlayerListCommand);
			try {
				ret = command.action(gameServer, this);
				ret.setCommand(command);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if ("move".equals(description)) {
			// a move should only be sent as a response to a request from the server
			assert (false);
			assert (command instanceof ServerMoveCommand);
			synchronized (commandWrapper) {
				commandWrapper.inputCommand = ((ServerMoveCommand)command).getMove();
				commandWrapper.notify();
				try {
					ret = command.action(gameServer, this);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ret.setCommand(command);
			}
			
		}

		return ret;
	}
	CommandWrapper commandWrapper = new CommandWrapper();
	
	@Override
	public Move getMove(Game g, Player p) {
		// Send a command to the client telling it to go.
		InputCommand command = null;
		try {
			NetworkResponse res = sendSynchronousCommand(new ClientGetMoveCommand("getmove"));
			ServerMoveCommand smc = (ServerMoveCommand) res.getCommand();
			command = smc.getMove();
			// wait for ack from
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		while(true) {
			if(command != null && command.shouldReturn()) {
				return command.dispatch(g, p);
			} else if (command != null){
				command.dispatch(g, p);
				System.out.println("FILL THIS IN!");
//				if(GuiMain.actionOccured.expectsReturn) {
//					GuiMain.actionOccured.notify();
//				}
			} else {
				System.out.println("Maximus says WTF!!!");
			}
		}
	}

	@Override
	public void handleException(Exception e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyBoardUpdate(Move m) {
		// TODO Auto-generated method stub
		
	}
	
	public NetworkResponse sendSynchronousCommand(NetworkCommand command) throws IOException {
		NetworkResponse retval = null;
		out.writeObject(command);
		try {
			// TODO: fix this to match the way Client works
			command.wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("GOT IT!");
		return retval;
	}
}
