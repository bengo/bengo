package network;

import game.Board;
import graphics.GuiMain;
import input.CommandMove;
import input.InputCommand;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.JOptionPane;

import network.ClientRxThread.ResponseWrapper;

import other.BoardCoordinate;
import other.Game;


public class ClientGame implements Runnable, Game {
	private Socket socket;
	private ObjectOutputStream out;
	LinkedBlockingQueue<InputCommand> inputCommands = new LinkedBlockingQueue<InputCommand>();
	LinkedBlockingQueue<InputCommand> outputCommands = new LinkedBlockingQueue<InputCommand>();	
	
	public ClientGame() {
		int n = Integer.parseInt(JOptionPane.showInputDialog("PORT!"));
		try {
			socket = new Socket();
			socket.setKeepAlive(true);
			socket.connect(new InetSocketAddress(n), 30);
			out = new ObjectOutputStream(socket.getOutputStream());
			ClientRxThread crt = new ClientRxThread(socket, out);
			new Thread(crt).start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		new Thread(this).start();
	}

	@Override
	public void run() {
		ClientConnectGameDialog ccgd = new ClientConnectGameDialog(null, this);
		ccgd.setVisible(true);
	}

	@Override
	public void play() {
		Board board = new Board(4);
		GuiMain gm = GuiMain.getInstance(board);
		gm.externalMain();
		// we can only send the move command as a response from a request to move given by the server
//		ServerMoveCommand smc = new ServerMoveCommand("move", new CommandMove(new BoardCoordinate(1,2)));
//		try {
//			sendSynchronousCommand(smc);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	public NetworkResponse sendSynchronousCommand(NetworkCommand command) throws IOException {
		NetworkResponse retval = null;
		// First synchronous the input and output threads
		pauseTraffic();
		out.writeObject(command);
		try {
			synchronized (ClientRxThread.clientResponseWrapper) {
				ClientRxThread.clientResponseWrapper.wait();
				retval = ClientRxThread.clientResponseWrapper.response;	
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		unpauseTraffic();
		return retval;
	}

	private void unpauseTraffic() {
		// TODO Auto-generated method stub
		
	}

	private void pauseTraffic() {
		// TODO Auto-generated method stub
		
		
	}

	@Override
	public Board getBoard() {
		// TODO Auto-generated method stub
		System.out.println("Implement me ");
		return null;
	}
}
