package network;

import input.InputCommand;

import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

public class ClientRxThread implements Runnable {

	class ResponseWrapper {
		NetworkResponse response;
	}
	static ResponseWrapper clientResponseWrapper;
	Socket server = null;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	
	public ClientRxThread(Socket server, ObjectOutputStream o) {
		super();
		clientResponseWrapper = new ResponseWrapper();
		this.server = server;
		try {
			in = new ObjectInputStream(server.getInputStream());
			out = o;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		while (server.isConnected()) {
			NetworkCommand command = null;
			NetworkResponse response = null;
			Object foo = null;
			try {
				foo = in.readObject();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (foo instanceof NetworkResponse) {
				response = (NetworkResponse) foo;
				synchronized (clientResponseWrapper) {
					clientResponseWrapper.response = response;
					clientResponseWrapper.notify();
				}
				continue;
			}
			
			command = (NetworkCommand) foo;
			try {
				out.writeObject((handleClientCommand(command)));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private NetworkResponse handleClientCommand(NetworkCommand command) {
		NetworkResponse ret = new NetworkResponse(true, "crap", command);
		return ret;
	}

}
